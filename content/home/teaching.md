+++
# An example of using the custom widget to create your own homepage section.
# To create more sections, duplicate this file and edit the values below as desired.

date = "2016-04-20T00:00:00"
draft = false

title = "Teaching"
subtitle = ""
widget = "custom"

# Order that this section will appear in.
weight = 60

+++

### Teaching Assistant
- Software Quality Metrics, _Autumn Semester 2016-2017_
- C/C++ Programming, _Autumn Semester 2016-2017_
- Fundamentals of Artificial Intelligence, _Autumn Semester 2017_
- Database Systems, _Spring Semester 2015-2017_

### Tutorials

- [Alternating Direction Method of Multipliers, August 2018](https://drive.google.com/open?id=1jav9GH_EGgbnwKanIZb_XBogCl9zQBxM "Slides")
- [Regularised Regression, July 2018](https://drive.google.com/open?id=1BOeQtrxVDQ0scmVpZXJCXLUXaJMwPxFw "Slides")
- [Multiobjective Optimisation, 2017](https://drive.google.com/open?id=1RXbSdo8aodgkspbwvlOFV8NhE0Watpcg "Slides")
