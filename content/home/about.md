+++
# About widget.

date = "2017-10-03T00:00:00"
draft = false

widget = "about"

# Order that this section will appear in.
weight = 1

# List your academic interests.
[interests]
  interests = [
    "Machine learning",
    "Artificial intelligence",
    "Numerical optimisation",
    "Reinforcement learning",
    "Applications: neuroimaging, wind energy, intelligent manufacturing"
  ]

# List your qualifications (such as academic degrees).

[[education.courses]]
  course = "PhD in Computer Science"
  institution = "University of Nottingham, UK"
  year = "2018"

[[education.courses]]
  course = "MSc (with Distinction) in Operational Research"
  institution = "University of Edinburgh, UK"
  year = 2013

[[education.courses]]
  course = "BEng in Software Engineering"
  institution = "Anhui University, China"
  year = 2008

+++

I am a Research Assocaite in Machine Learning Group, University of Sheffield, working with <a href="http://staffwww.dcs.shef.ac.uk/people/H.Lu/">Dr. Haiping Lu</a> on the project -- Learning Sparse Features from 4D fMRI Data for Brain Disease Diagnosis (EP/R014507/1). The project aims to develop a new tensor-based machine learning method for dealing with severe cases of "large p, small n" for multidimensional data such as whole-brain fMRI. 

I obtained the PhD degree in Computer Science from the Automated Scheduling, Optimisation and Planning (ASAP) research group, University of Nottingham, UK 2018, under the supervision of Dr. <a href="http://www.cs.nott.ac.uk/~pszeo/">Ender Özcan </a> and <a href="http://www.cs.nott.ac.uk/~pszeo/"> Prof. Robert John </a>. I have five years’ industry work experience in telecommunication, airline, finance, and manufacturing sectors (more details can be found in my <a href="https://drive.google.com/open?id=1zfC83wKimzvGLWvngTnh_yhvYR_WYmkJ">CV</a>).

I am currently looking for jobs in machine learning, artifical intelligence and optimisation.
