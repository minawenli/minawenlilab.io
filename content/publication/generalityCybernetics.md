+++
title = "A Generality Analysis of Learning Automata based Multiobjective Selection Hyper-heuristics"
#date  = 2018-08-01
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors=["W. Li", "E. Özcan", "M. Maashi", "Robert John"]

# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference proceedings
# 2 = Journal
# 3 = Work in progress
# 4 = Technical report
# 5 = Book
# 6 = Book chapter


publication_types = ["3"]

# Publication name and optional abbreviated version.
publication = "IEEE Transactions on Cybernetics (submitted)"

preview=false
# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++