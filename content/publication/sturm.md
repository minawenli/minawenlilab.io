+++
title = "“Sturm: Sparse Tubal-Regularized Multilinear Regression for fMRI"
#date  = 2018-08-01
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors=["W. Li", "J. Lou", "S. Zhou", "H. Lu"]

# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference proceedings
# 2 = Journal
# 3 = Work in progress
# 4 = Technical report
# 5 = Book
# 6 = Book chapter


publication_types = ["3"]

# Publication name and optional abbreviated version.
publication = "Conference (submitted)"

# Links (optional)
url_pdf = "https://arxiv.org/pdf/1812.01496.pdf"
preview=false
# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++