+++
title = "A learning automata based multiobjective hyper-heuristic"
date  = 2018-12-04
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors=["W. Li", "E. Özcan", "R. John"]

# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference proceedings
# 2 = Journal
# 3 = Work in progress
# 4 = Technical report
# 5 = Book
# 6 = Book chapter

publication_types = ["2"]

# Publication name and optional abbreviated version.
publication = "IEEE Transactions on Evolutionary Computation"
#publication_short = "IEEE Trans. Evol. Comput."

# Abstract and optional shortened version.
abstract = "Metaheuristics, being tailored to each particular domain by experts, have been successfully applied to many computationally hard optimisation problems. However, once implemented, their application to a new problem domain or a slight change in the problem description would often require additional expert intervention. There is a growing number of studies on reusable cross-domain search methodologies, such as, selection hyper-heuristics, which are applicable to problem instances from various domains, requiring minimal expert intervention or even none. This study introduces a new learning automata based selection hyper-heuristic controlling a set of multiobjective metaheuristics. The approach operates above three well-known multiobjective evolutionary algorithms and mixes them, exploiting the strengths of each algorithm. The performance and behaviour of two variants of the proposed selection hyper-heuristic, each utilising a different initialisation scheme are investigated across a range of unconstrained multiobjective mathematical benchmark functions from two different sets and the realworld problem of vehicle crashworthiness. The empirical results illustrate the effectiveness of our approach for cross-domain search, regardless of the initialisation scheme, on those problems when compared to each individual multiobjective algorithm. Moreover, both variants perform signicantly better than some previously proposed selection hyper-heuristics for multiobjective optimisation, thus signicantly enhancing the opportunities for improved multiobjective optimisation."
abstract_short = ""

# Does this page contain LaTeX math? (true/false)
math = false

# Does this page require source code highlighting? (true/false)
highlight = true

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Links (optional)
url_pdf = "http://eprints.nottingham.ac.uk/48692/1/IEEE%20TEC%20Accepted%20Version.pdf"
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""

[[url_custom]]
    name = "DOI"
    url = "http://dx.doi.org/10.1109/TEVC.2017.2785346"

preview=false
# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++
