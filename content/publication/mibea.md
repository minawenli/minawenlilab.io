+++
title = "A modified indicator-based evolutionary algorithm (mIBEA)"
date  = 2017-06-05
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors=["W. Li", "E. Özcan", "R. John", "J. H. Drake", "A. Neumann", "M. Wagner"]

# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference proceedings
# 2 = Journal
# 3 = Work in progress
# 4 = Technical report
# 5 = Book
# 6 = Book chapter

publication_types = ["1"]

# Publication name and optional abbreviated version.
publication = "IEEE Congress on Evolutionary Computation (CEC), pp. 1047-1054"
#publication_short = "CEC"

# Abstract and optional shortened version.
abstract = "Multi-objective evolutionary algorithms (MOEAs) based on the concept of Pareto-dominance have been successfully applied to many real-world optimisation problems. Recently, research interest has shifted towards indicator-based methods to guide the search process towards a good set of trade-off solutions. One commonly used approach of this nature is the indicator-based evolutionary algorithm (IBEA). In this study, we highlight the solution distribution issues within IBEA and propose a modification of the original approach by embedding an additional Pareto-dominance based component for selection. The improved performance of the proposed modified IBEA (mIBEA) is empirically demonstrated on the well-known DTLZ set of benchmark functions. Our results show that mIBEA achieves comparable or better hypervolume indicator values and epsilon approximation values in the vast majority of our cases (13 out of 14 under the same default settings) on DTLZ1-7. The modification also results in an over 8-fold speed-up for larger populations."
abstract_short = ""

# Does this page contain LaTeX math? (true/false)
math = false

# Does this page require source code highlighting? (true/false)
highlight = true

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Links (optional)
url_pdf = "http://eprints.nottingham.ac.uk/41420/1/A%20Modified%20Indicator-based%20Evolutionary%20Algorithm%20%28mIBEA%29%20final%20submission.pdf"
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""

[[url_custom]]
    name = "DOI"
    url = "https://dx.doi.org/10.1109/CEC.2017.7969423"

preview=false
# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++
