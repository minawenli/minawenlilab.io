+++
title = "To Kit or Not to Kit: Analysing the Value of Model-based Kitting for Additive Manufacturing"
date  = 2018-08-01
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors=["S. Khajavi", "M. Baumers", "J. HolmstrÖm", "E. Özcan", "J. Atkin", "W. Jackson", "W. Li"]

# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference proceedings
# 2 = Journal
# 3 = Work in progress
# 4 = Technical report
# 5 = Book
# 6 = Book chapter


publication_types = ["2"]

# Publication name and optional abbreviated version.
publication = "Computers in Industry vol. 98, pp. 100–117"
#publication_short = "Computers in Industry"

# Abstract and optional shortened version.
abstract = "The use of additive manufacturing (AM) for the production of functional parts is increasing. Thus, AM based practices that can reduce supply chain costs gain in importance. We take a forward-looking approach and study how AM can be used more effectively in the production of multi-part products in low to medium quantities. The impact of introducing kitting in AM on supply chain cost is investigated. Kitting approaches are traditionally devised to feed all components belonging to an assembly into individual containers. Where conventional manufacturing approaches are used for kitting, the produced parts pass through inventory and a kit preparation step before being forwarded to the assembly line/station. However, by taking advantage of the object-oriented information handling inherent in the AM process, kitting information can be embedded directly within the digital design data and parts produced in a common build. This model-based kitting practice reduces-even eliminates-the need for a manual kit preparation step and promises additional supply chain benefits. Eight experiments were conducted using laser sintering (LS) to investigate the impact of model-based component kitting on production cost and supply chain cost. The results show that with current state-of-the art volume packing software production costs increase with the adoption of kitting. The increased production cost, was offset by benefits, including simplified production planning, reduced work-in-progress inventory and elimination of parts fetching prior to assembly. Findings of this research are of interest for manufacturers, service bureaus and practitioners who use AM for low quantity production, as well as developers of AM volume packing and production planning software."
abstract_short = ""

# Does this page contain LaTeX math? (true/false)
math = false

# Does this page require source code highlighting? (true/false)
highlight = true

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Links (optional)
url_pdf = "http://www.cs.nott.ac.uk/~pszeo/docs/publications/toKit.pdf"
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""

[[url_custom]]
    name = "DOI"
    url = "https://doi.org/10.1016/j.compind.2018.01.022"

preview=false
# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++
