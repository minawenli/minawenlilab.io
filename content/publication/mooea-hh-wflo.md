+++
title = "Multi-objective evolutionary algorithms and hyper-heuristics for wind farm layout optimisation"
date  = 2017-05-01
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors=["W. Li", "E. Özcan", "R. John"]

# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference proceedings
# 2 = Journal
# 3 = Work in progress
# 4 = Technical report
# 5 = Book
# 6 = Book chapter

publication_types = ["2"]

# Publication name and optional abbreviated version.
publication = "Renewable Energy vol. 105, pp. 473-482"
#publication_short = ""

# Abstract and optional shortened version.
abstract = "Wind farm layout optimisation is a challenging real-world problem which requires the discovery of trade-off solutions considering a variety of conflicting criteria, such as minimisation of the land area usage and maximisation of energy production. However, due to the complexity of handling multiple objectives simultaneously, many approaches proposed in the literature often focus on the optimisation of a single objective when deciding the locations for a set of wind turbines spread across a given region. In this study, we tackle a multi-objective wind farm layout optimisation problem. Different from the previously proposed approaches, we are applying a high-level search method, known as selection hyper-heuristic to solve this problem. Selection hyper-heuristics mix and control a predefined set of low-level (meta)heuristics which operate on solutions. We test nine different selection hyper-heuristics including an online learning hyper-heuristic on a multi-objective wind farm layout optimisation problem. Our hyper-heuristic approaches manage three well-known multi-objective evolutionary algorithms as low-level metaheuristics. The empirical results indicate the success and potential of selection hyper-heuristics for solving this computationally difficult problem. We additionally explore other objectives in wind farm layout optimisation problems to gain a better understanding of the conflicting nature of those objectives."
abstract_short = ""

# Does this page contain LaTeX math? (true/false)
math = false

# Does this page require source code highlighting? (true/false)
highlight = true

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = false

# Links (optional)
url_pdf = "http://eprints.nottingham.ac.uk/39371/1/MO_windfarm.pdf"
url_code = ""
url_dataset = ""
url_project = ""
url_slides = ""
url_video = ""

[[url_custom]]
    name = "DOI"
    url = "https://doi.org/10.1016/j.renene.2016.12.022"

preview=false
# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++
