+++
title = "Multiobjective evolutionary algorithms and reinforcement learning-based hyper-heuristics"
date = 2017-10-02
draft = false

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = ["reinforcement-learning", "multiobjective-optimisation", "evolutionary-algorithms", "hyper-heuristics"]

# Project summary to display on homepage.
summary = "This project has two primary foci. First, to gain deep understanding of the pros and cons of different types of multiobjective evolutionary algorithms (MOEAs) and the influences on their performance on both mathematical benchmarks and real-world problems. For example, wind farm layout optimisation and the vehicle crashworthiness problems. The second seeks to design a high-level method to combine the strengths of different MOEAs in an online learning manner. To achieve this goal, a reinforcement learning-based multiobjetive selection hyper-heuristic framework is designed and tested on both benchmark and real-world problems. The experimental results demonstrated the effectiveness and generality of the proposed hyper-heuristic framework."

# Optional image to display on homepage.
image_preview = ""

# Optional external URL for project (replaces project detail page).
external_link = ""

# Does the project detail page use math formatting?
math = false

# Does the project detail page use source code highlighting?
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++

This project has two primary foci. First, to gain further understanding of the pros and cons of different types of multiobjective evolutionary algorithms (MOEAs) and the influences on their performance on both mathematical benchmarks and real-world problems. The second seeks to design a high-level method to combine the strengths of different MOEAs in an online learning manner. To achieve this goal, a reinforcement learning-based multiobjetive selection hyper-heuristic framework is designed and tested on both benchmark and real-world problems.
